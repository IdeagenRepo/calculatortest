﻿using System;
using System.Data;

namespace IdeagenCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Input:");
            string input = Console.ReadLine();//read input string
            
            Console.WriteLine("Sum: " + Calculate(input));
            Console.WriteLine("Press any key to stop");
            Console.ReadKey();
        }

        /// <summary>
        /// To calculate the total/sum based on the given input string.
        /// Support bracket and nested brackets.
        /// Candidate: Azhar Bin Abdul Rashid
        /// Email: azharabdulrashid88@gmail.com
        /// Time to complete : 10 minutes.
        /// </summary>
        /// <param name="sum">input string</param>
        /// <returns>the total/sum based on the given input string</returns>
        public static double Calculate(string sum)
        {
            //use DataTable().Compute() to process string expression.
            return Convert.ToDouble(new DataTable().Compute(sum, string.Empty));
        }
    }
}
